#!/usr/bin/python3
import pandas as pa
import json

file = "zap_report.json"
columns = ['VulnerabilityID', 'Title', 'PkgName', 'InstalledVersion', 'FixedVersion', 'Severity']

def parse_zap_results():
    with open(file,'r') as f:
        data = json.loads(f.read())['site'][0]['alerts']
 
    df = pa.DataFrame(data)

    data = df[['cweid','alert','riskcode']]
 #   print(df.columns)
 
    new_df = pa.DataFrame(columns=columns)
    new_df[['Title', 'Severity']] = data[['alert','riskcode']]
    new_df['VulnerabilityID'] = 'CWE-Id: ' + data['cweid']
    new_df['Severity']=new_df['Severity'].replace(['0','1','2','3'],['LOW','MEDIUM','HIGH','CRITICAL'])

    
    new_df = new_df.sort_values(by= ['Severity'], ascending = True)
    new_df['Tool'] = ['ZAP API Scan' for count in range(len(new_df.values))]
    new_df = new_df.fillna('-')
#   print(new_df)
    new_df.to_csv("zap_results.csv", index=False)

if __name__ == "__main__":
    parse_zap_results()