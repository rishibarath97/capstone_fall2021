import os
import pandas as pd
import yaml

def combine():
    '''
    Function to combine all the CSV files in the directory into one master CSV
    '''

    files = [file for file in os.listdir() if "csv" in file]
    df = pd.concat([pd.read_csv(file) for file in files])

    dropwords = yaml.safe_load(open("tools/analysis/exclude.yml", 'rb'))

    for key in dropwords:
        if(key == 'Row'):
            # add code to drop row #
            continue
            
        if(dropwords[key] != [None]):
            test = df[~df[key].isin(dropwords[key])]

    try:
        test['Image'] = [os.environ['IMAGE'] for count in range(len(test.values))]  # adding the name of the image from the environment variables
        test.to_csv("results.csv", sep=";")
    except UnboundLocalError:
        df['Image'] = [os.environ['IMAGE'] for count in range(len(df.values))]  # adding the name of the image from the environment variables
        df.to_csv("results.csv", sep=";")

if __name__ == "__main__":
    combine()