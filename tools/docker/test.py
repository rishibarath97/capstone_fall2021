import subprocess
import pandas as pd

columns = ['VulnerabilityID', 'Title', 'PkgName', 'InstalledVersion', 'FixedVersion', 'Severity']    # Set columns
vuls = []

def test_socker():
        '''
        Test docker socket with Break Out Of The Box
        '''

        socket_command = "./botb-linux-amd64 -find-sockets=True | grep \"docker.sock\""                         # Command to run
        process = subprocess.run(socket_command.split(), stdout=subprocess.PIPE)
        output = process.stdout.decode('utf-8').split("\n")
        if(len(output[2:-2]) > 1):
                for socket in output[2:-2]:
                        if("docker" in socket):
                                print("Found DOCKER SOCKET AT",socket.split(":")[-1].strip())
                                vuls.append(['-', 'Docket socker found at {}'.format(socket.split(":")[-1].strip()), '-', '-', '-', 'CRITICAL'])
        else:
                print("Cannot break out with docker socket. No socket exists!")


def test_privilege_flag():
        '''
        Test privilege flag exploit with fdisk command
        '''

        privilege_command = "fdisk -l"
        try:
                process = subprocess.run(privilege_command.split(), stdout=subprocess.PIPE)
                errorcode = process.returncode
                output = process.stdout.decode('utf-8')
                print(output)
        except FileNotFoundError:
                errorcode = 0

        if(errorcode == 0):
                print("Does not have privilege flag mounting capabilities")
        else:
                print("HAS PRIVILEGE FLAG CAPABILITIES. Can break out of docker!")
                vuls.append(['-', 'Privilege flag capabilities', '-', '-', '-', 'CRITICAL'])

def set_df():
        '''
        Function to prepare CSV
        '''

        df = pd.DataFrame(vuls, columns = columns)
        df['Tool'] = ['Docker Test' for count in range(len(vuls))]
        df.to_csv("docker_test.csv", index = False)

def main():
        test_socker()
        test_privilege_flag()
        set_df()

if __name__ == "__main__":
        main()
