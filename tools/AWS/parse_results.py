import boto3
import pandas as pd
import os

iam = boto3.client('iam')
docs = []
policies = []

username = os.environ['AWS_USER']
attached_policies = iam.list_attached_user_policies(UserName=username)['AttachedPolicies']
for policy in attached_policies:
	policyArn = policy['PolicyArn']
	policyName = policy['PolicyName']
	
	policyMetadata = iam.get_policy(PolicyArn=policyArn)
	policyVersion = policyMetadata['Policy']['DefaultVersionId']
	policyDocument = iam.get_policy_version(PolicyArn=policyArn, VersionId=policyVersion)
	
	temp = policyDocument['PolicyVersion']['Document']['Statement']

	for t in temp:
		t['PolicyName'] = policyName

	docs = docs + temp

df = pd.DataFrame(docs).fillna("-")

for row in df.values:
	if(isinstance(row[1], list)):
		for action in row[1]:
			temp = [row[0], action, row[2], row[3], row[4]]
			policies.append(temp)
	else:
		policies.append(row)

df = pd.DataFrame(policies, columns = ['Effect', 'Action', 'Resource', 'Policy Name', 'SID'])
df['Resource'] = [", ".join(resource) if isinstance(resource, list) else resource for resource in df['Resource']]
df.to_csv("policies.csv", index=False, sep=";")