import pandas as pd
import json

file = "container-scanning-report.json"
columns = ['VulnerabilityID', 'Title', 'PkgName', 'InstalledVersion', 'FixedVersion', 'Severity']

def parse_trivy_results():
    '''
    Read Trivy Scan json and parse it into a CSV
    '''
    
    f = json.loads(open(file, "rb").read())[0]
    df = pd.DataFrame(f['Vulnerabilities'])[columns].dropna()
    df = df.sort_values(by = ['VulnerabilityID'], ascending = False)

    crit, med = df[df['Severity'] == 'CRITICAL'], df[df['Severity'] == 'MEDIUM']
    vul = pd.concat([crit, med])

    vul['Tool'] = ['Trivy Scan' for count in range(len(vul.values))]

    vul.to_csv("trivy_results.csv", index=False)                      

if __name__ == "__main__":
    parse_trivy_results()