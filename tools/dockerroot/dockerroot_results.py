#!/usr/bin/python3

import pandas as pd

file = "dockerroot_results.txt"
columns = ['VulnerabilityID', 'Title', 'PkgName', 'InstalledVersion', 'FixedVersion', 'Severity', 'Tool']

def parse_dockerroot_results():

#read results
 '''
    Reading if Root was Obtained
 '''
with (open(file,"r")) as f:
    line = f.readlines()

error = "==== ERROR ===="

#include conditional statement to match severity
if ( line[1] != error ):
    vul = [['-', line[1].strip(),'-', '-','-', 'CRITICAL', 'DockerRootPlease'] ]
else:
    vul = [['-', line[1].strip(),'-', '-','-', '-', 'DockerRootPlease'] ]

df = pd.DataFrame(vul, columns = columns)
df.to_csv("dockerroot_results.csv", index=False)

if __name__ == "__main__":
    parse_dockerroot_results()