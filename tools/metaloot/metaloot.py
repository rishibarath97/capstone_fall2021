import requests
from bs4 import BeautifulSoup as soup
import pandas as pd

def get_metadata():
    '''
    Query EC2 metadata service to parse metadata
    '''

    root = "http://169.254.169.254/latest/meta-data/"                                   # Metadata service URL
    page = requests.get(root)
    content = soup(page.content, "html.parser")
    metadata = []
    req_fields = open("tools/metaloot/req_fields.txt", 'r').read().strip().split("\n")  # Getting required fields from external input

    for directory in req_fields:
        link = root + directory
        sub_content = soup(requests.get(link).content, "html.parser")
        temp = {'Field':directory, 'Value':sub_content}
        metadata.append(temp)

    df = pd.DataFrame(metadata)
    df.to_csv("metadata.csv", index="False")

if __name__ == "__main__":
    get_metadata()