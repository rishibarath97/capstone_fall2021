#!/usr/bin/python3

import os
import yaml
import pandas as pd

keyword_file = "tools/python/keywords.yml"
columns = ['VulnerabilityID', 'Title', 'PkgName', 
            'InstalledVersion', 'FixedVersion', 'Severity']                              # Setting columns
vul = []

def get_env_variables():
    env_variables = {item:value for item, value in os.environ.items()}
    keywords = yaml.safe_load(open(keyword_file, 'rb'))                                     # Loading keywords list from yaml

    for word in keywords:
            for k in env_variables:
                if('True' in set(['True' if _ in k else 'False' for _ in keywords[word]])):
                    vul.append(['-', k + " shown in plaintext:" + env_variables[k], 
                                '-', '-', '-', word.upper()])
                
def main():
    get_env_variables()
    df = pd.DataFrame(vul, columns = columns)
    df['Tool'] = ['Environment Variables' for count in range(len(df.values))]
    df.to_csv("python_job.csv", index=False)

if __name__ == "__main__":
    main()