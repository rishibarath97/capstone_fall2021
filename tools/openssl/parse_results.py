from datetime import datetime
import pandas as pd
import os

def parse():
    vuln = []
    columns = ['VulnerabilityID', 'Title', 'PkgName', 'InstalledVersion', 'FixedVersion', 'Severity']

    certificate = open("openssl.txt").read().split("\n")[2:]
    certificate_parsed = [_.strip() for _ in certificate]

    certificate_parsed[1] = "{} {}".format(certificate_parsed[1], certificate_parsed[2])
    certificate_parsed.remove(certificate_parsed[2]); certificate_parsed.remove(certificate_parsed[4])

    certificate_json = {k.split(":")[0].strip(): ":".join(k.split(":")[1:]).strip() for k in certificate_parsed}

    if(certificate_json['Signature Algorithm'] != os.environ['ALGORITHM']):
        vuln = vuln + [['-', 'Encryption Algorithm is not what is intended. Currently present: ' + certificate_json['Signature Algorithm'], 'openssl x509', '-', '-', 'CRITICAL']]
        
    if(certificate_json['Subject'] != "CN=*.{}.com".format(os.environ['ISSUER_CN'])):
        vuln = vuln + [['-', 'Issuer name is not what is intended. Currently present ' + certificate_json['Subject'], 'openssl x509', '-', '-', 'CRITICAL']]

    after = datetime.today()-datetime.strptime(certificate_json['Not Before'], "%b %d %H:%M:%S %Y %Z")
    before = datetime.today()-datetime.strptime(certificate_json['Not After'], "%b %d %H:%M:%S %Y %Z")

    if(after.days <= 0):
        string = "Invalid TLS certificate date: Certificate is not yet active (start date: {})".format(certificate_json['Not Before'])
        vuln = vuln + [['-', string, 'openssl x509', '-', '-', 'CRITICAL']]
        
    if(before.days >= 0):
        string = "Invalid TLS certificate date: Certificate has expired (end date: {})".format(certificate_json['Not After'])
        vuln = vuln + [['-', string, 'openssl x509', '-', '-', 'CRITICAL']]
        
    df = pd.DataFrame(vuln, columns = columns)
    df.to_csv("openssl.csv", index=False)

if __name__ == "__main__":
    parse()