# Capstone_Fall2021

<h2> Overview </h2>

<p> This project showcases CI/CD security by creating a template for testing the CI/CD environment and the image used in the environment The pileine uses a whole host of tools to cover all the bases, including testing the environment variables for hidden secrets to testing the docker image in the pipeline using industry standard tools like Trivy Scan from AquaSec.</p>

<h2> Tools included </h2>

<ul>
    <li> AWS Environment Variables Parser </li>
    <li> AMIContained: A docker inspection tool </li>
    <li> Docker socket and mount tool </li>
    <li> AWS EC2 metadata parser </li>
    <li> AquaSec's Trivy Scan tool for docker image </li>
    <li> OpenSSL Certificate Test </li>
    <li> NMap network test </li>
    <li> OWASP Zap test for docker image </li>
    <li> Falco run-time deployment </li>
</ul>

<h2> Configuration </h2>

<p> The project requires an AWS IAM role to be set and the keys passed. The following is the Policy Document of the required permissions </p>

IAM ReadOnly Access (An AWS managed policy)
<pre>
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "iam:GenerateCredentialReport",
                "iam:GenerateServiceLastAccessedDetails",
                "iam:Get*",
                "iam:List*",
                "iam:SimulateCustomPolicy",
                "iam:SimulatePrincipalPolicy"
            ],
            "Resource": "*"
        }
    ]
}
</pre>
<br>A policy to write the results to an S3 bucket of choice <br>
<pre>
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObjectAcl",
                "s3:GetObject",
                "s3:GetBucketTagging",
                "s3:ListBucketVersions",
                "s3:ListBucket",
                "s3:GetBucketVersioning",
                "s3:GetBucketAcl",
                "s3:GetBucketPolicy"
            ],
            "Resource": [
                "ARN of S3 bucket/*",
                "ARN of S3 bucket"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "*"
        }
    ]
}
</pre>
The service role used to run the pipeline must have these permissions

<p> Set the name of the S3 bucket  and the docker hub syntax of the image to be tested in the CI/CD variables section of GitLab </p>

<h2> Members of the Team </h2>

<p> The project was done in collaboration with <a href="https://climate.com/bayer-plus/"> The Climate Corp</a> and their Sr. Security Engineering Manager: <a href="https://www.linkedin.com/in/zoto1/">Zot O' Connor</a> </p>

<ul>
    <a href="https://www.linkedin.com/in/rishibarathb/"><li> Rishi Barath Balaji</a> </li>
    <a href="https://www.linkedin.com/in/benjamin-aryee/"><li> Benjamin Aryee</a> </li>
    <a href="https://www.linkedin.com/in/swetha-mile/"><li> Swetha Mile</a> </li>
    <a href="https://www.linkedin.com/in/arjunkatneni/"><li> Arjun Katneni</a> </li>
</ul>

<p> All four members are Graduates of Northeastern University from Boston, Massachusetts. </p>

<h2> Reference Links: </h2>
<ul>
    <a href="https://docs.gitlab.com/ee/ci/cloud_deployment/"><li> Cloud Deployment doc</li></a>
    <a href="https://docs.gitlab.com/ee/ci/examples/"><li> GitLab CI/CD examples</li></a>
    <a href="https://www.1strategy.com/blog/2021/02/16/deploying-gitlab-runners-on-ec2/"><li> GitLab runners on EC2</li></a>
    <a href="https://theoffensivelabs.medium.com/docker-escape-using-mounted-docker-sock-6d5a74d6b783"><li> Docker Socket escape</li></a>
    <a href="https://cheatsheetseries.owasp.org/cheatsheets/Docker_Security_Cheat_Sheet.html"><li> OWASP Docker Security</li></a>
    <a href="https://book.hacktricks.xyz/linux-unix/privilege-escalation/docker-breakout"><li> Docker BreakOut techniques</li></a>
    <a href="https://github.com/awgh/amicontained"><li> AMIContained</li></a>
    <a href="https://medium.com/mitre-engenuity/att-ck-for-containers-now-available-4c2359654bf1"><li> MITRE ATT&CK Framework for docker</li></a>
    <a href="https://docs.gitlab.com/runner/install/linux-repository.html"><li> Setting up Gitlab-runner on Aws EC2 Linux Free Tier </li></a>
    <a href="https://microfluidics.utoronto.ca/gitlab/help/ci/docker/using_docker_build.md"><li> Setting up Gitlab-runner on Aws EC2 Linux Free Tier - Add docker group</li></a>
    <a href="https://linuxhint.com/install_configure_docker_ubuntu/"><li> Installing docker on Gitlab-Runner</li></a>
    <a href="https://www.marcolancini.it/2018/blog-arsenal-cloud-native-security-tools/"><li>  Security Tools from Marco Lancini</li></a>
    <a href="https://geekflare.com/container-security-scanners/"><li> GeekFlare Securtiy Scanner Tools</li></a>
</ul>
